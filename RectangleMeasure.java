import java.util.Scanner;
public class RectangleMeasure {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("If you want me to help you get the measures of your rectangle, please insert its base.");
        double base = input.nextDouble();

        System.out.println("Now insert your width.");
        double width = input.nextDouble();

        double perimeter = (2*base)+(2*width);
        double area = base*width;
    
        System.out.println("The perimeter of the rectangle is " + perimeter);
        System.out.println("The area of the rectangle is " + area);
    }
}