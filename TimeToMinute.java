import java.util.Scanner;
import java.util.*;

public class TimeToMinute{
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("Please insert any amount of seconds and I'll convert it to minutes.");

        int totalSeconds = input.nextInt();
        int minutes = totalSeconds / 60;
        int seconds = totalSeconds % 60;

        System.out.println("\n" +totalSeconds+ " seconds is equal to " +minutes+":"+seconds+" minutes.");
    }
}
