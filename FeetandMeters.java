import java.util.Scanner;

public class FeetandMeters{
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);

		System.out.println("Please input the amount of feet to convert.");
		Double feet = input.nextDouble();
        Double meter = feet * 0.304;
        System.out.printf("\n%f feet is equal to %f meters.\n",feet,meter);

        System.out.printf("Now, insert the amount of meters to feet ");
        Double newmeters = input.nextDouble();
        Double newfeet = newmeters * 3.28;
        System.out.printf("\n%f meters is equal to %f feet.\n", newmeters, newfeet);
	}
}