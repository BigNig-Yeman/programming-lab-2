import java.util.*;

public class YourNameIs{
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Type 'lastname, firstname");
        String firstFormat = input.nextLine();

        int indexComma = firstFormat.indexOf(',');
        String last = firstFormat.substring(0, indexComma);
        String first = firstFormat.substring(indexComma + 2);

        last = last.substring(0, 1).toUpperCase() + last.substring(1).toLowerCase();
        first = first.substring(0, 1).toUpperCase() + first.substring(1).toLowerCase();

        System.out.printf("Your first name is %s, correct?\n", first);
        System.out.printf("That means that your last name is %s.\n", last);
        System.out.printf("Your name is %s %s, right? Nice to meet you!", 
                first, last);
    }
}
