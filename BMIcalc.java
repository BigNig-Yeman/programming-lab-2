import java.util.Scanner;

public class BMIcalc{
	public static void main(String[] args){

		System.out.println("Good day. In order for me to figure out your BMI (Body Mass Index), I'll need to know your height and weight");

		Scanner input = new Scanner(System.in);
        System.out.println("First up, how many pounds do you weigh?");
        Double pounds = input.nextDouble();
        Double kilo = pounds * 0.45;

        System.out.println("That's good! Now for the height (in feet please).");
        Double height = input.nextDouble();
        Double meter = height * 0.304;

        Double bmi = kilo/(meter*meter);
        System.out.println("So, according to my calculations, your BMI is around "+bmi);
        System.out.println("That's a pretty nice result. Could be better, but don't let that bring you down! ");
	}
}