import java.util.*;
import java.util.Scanner;

public class AverageTemperature{
	public static void main(String[] args){

		Scanner input = new Scanner(System.in);
        System.out.println("Would you kindly tell me what the weather was yesterday? Preferably in Celsius.");
        Double firstdaytemp = input.nextDouble();

        System.out.println("\nThank you. Now what was the temperature like 2 days ago. Also in celsius please.");
        Double seconddaytemp = input.nextDouble();

        System.out.println("\nGot it. Now, one last question, how was the weather like 3 days ago? Obviously not in Fahrenheit");
        Double thirddaytemp = input.nextDouble();

        Double averagetemperature = (firstdaytemp + seconddaytemp + thirddaytemp)/3;
        System.out.println("\nI see, it would appear that the average temperature of the last 3 days was around \n" +averagetemperature+ " degrees Celsius. Thank you for time!");
	}
}